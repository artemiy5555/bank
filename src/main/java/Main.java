import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    private final static CountDownLatch latch = new CountDownLatch(1_000);
    private final static Lock lockerDepos = new ReentrantLock();
    private final static Lock lockerCredit = new ReentrantLock();
    private final static Lock lockerVerifier = new ReentrantLock();
    private static boolean work = true;
    static void setWork(boolean work) {
        Main.work = work;
    }
    static CountDownLatch getLatch() {
        return latch;
    }

    public static void main(String[] args) {
        Bank bank = new Bank();
        Casir casir = new Casir();
        casir.start(lockerDepos);
        Verifier verifier = new Verifier();
        verifier.start(lockerVerifier);
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleWithFixedDelay(()->client(bank), 1, 5, TimeUnit.MILLISECONDS);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("await stop (" + latch + ")");
        executorService.shutdownNow();

        while (Thread.getAllStackTraces().keySet().size() != 7) {
//            System.out.println(Thread.getAllStackTraces().keySet().size());
        }
        casir.stop();
    }

    private static void client(Bank bank){
        new Thread(() -> {
            while (true) {
                if (work) {
                    int rand = 1 + (int) (Math.random() * 2);

                    switch (rand) {
                        case 1:
                            System.out.println("Создаем кридетера");
                            synchronized (lockerCredit) {
                                try {
                                    int kredit = (int) (Math.random() * 20_000);
                                    while (true) {
                                        if (bank.getCash() - kredit >= 0) {
                                            break;
                                        } else {
                                            System.out.println("Ждем денег");
                                            lockerCredit.wait();
                                        }
                                    }
                                    bank.setCash(bank.getCash() - kredit);
                                    System.out.println("кэш после кредита -- " + bank.getCash());
                                    lockerCredit.wait();
                                    bank.setCash((int) (bank.getCash() + (kredit * 1.4)));
                                    System.out.println("кэш после прихода колектора -- " + bank.getCash());
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            synchronized (lockerDepos) {
                                lockerDepos.notify();
                            }
                            break;
                        case 2:
                            System.out.println("Создаем депозитчика");
                            synchronized (lockerDepos) {

                                try {
                                    int depos = (int) (Math.random() * 20_000);

                                    while (true) {
                                        if (bank.getCash() + depos <= 1_000_000) {
                                            break;
                                        } else {
                                            System.out.println("Ждем кридита");
                                            lockerDepos.wait();
                                            if (latch.getCount() == 0) {
                                                break;
                                            }
                                        }
                                    }
                                    bank.setCash(bank.getCash() + depos);
                                    System.out.println("кэш после депозита -- " + bank.getCash());
                                    Thread.sleep(100);
                                    while (true) {
                                        if (bank.getCash() - depos * 1.1 >= 0) {
                                            break;
                                        } else {
                                            System.out.println("Ждем денег");
                                            lockerDepos.wait();
                                        }
                                    }
                                    bank.setCash((int) (bank.getCash() - depos * 1.1));
                                    System.out.println("кэш после возврата депозита -- " + bank.getCash());
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            synchronized (lockerCredit) {
                                lockerCredit.notifyAll();
                            }
                            break;
                        default:
                            break;

                    }
                    break;
                } else {
                    synchronized (lockerVerifier) {
                        try {
                            lockerVerifier.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }

        }}).start();

        if (latch.getCount() > 0) {
            latch.countDown();
            //System.out.println("client countDown {" + latch.getCount() + "}");
        }

    }

}