import java.util.concurrent.locks.Lock;

class Casir {

    private  Thread thread;

     void start(Lock lockerDepos) {
        try {
             thread =new Thread(() -> {
                while (true) {
                    System.out.println("casir start");
                    synchronized (lockerDepos) {
                        lockerDepos.notifyAll();
                    }
                    System.out.println("casir end");
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
             thread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     void stop(){
        thread.stop();
    }

}
